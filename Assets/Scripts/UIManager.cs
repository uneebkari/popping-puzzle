using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public GameObject mainMenuPanel;
    public GameObject levelSelctionPanel;
    
    public Button[] levelsBtns;
    public Button unlockLevelsBtn;
    public static bool isComingForLevelSelction = false;

    public Levels[] _levels;
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("once"))
        {
            PlayerPrefs.SetInt("once", 1);
            PlayerPrefs.SetInt("levelsUnlocked", 1);
        }
    }
    private void Start()
    {

        if (isComingForLevelSelction) {
            isComingForLevelSelction = true;
            GoToLevelSelection();
        }
    }
    public void GoToLevelSelection() {
        if (PlayerPrefs.GetInt("levelsUnlocked", 1) > levelsBtns.Length)
        {
            PlayerPrefs.SetInt("levelsUnlocked", levelsBtns.Length);
        }

        int unlockedLevel = PlayerPrefs.GetInt("levelsUnlocked");
        Debug.Log(unlockedLevel + "    <<<<<<<<<<<<<<<");

        for (int i = 0; i < unlockedLevel; i++)
        {
            levelsBtns[i].interactable = true;
        }
        mainMenuPanel.SetActive(false);
        levelSelctionPanel.SetActive(true);
        if (unlockedLevel == 6) {
            unlockLevelsBtn.gameObject.SetActive(false);
        }
    }
  

    public void PlayThisLevel(int levelNumber) {
        PlayerPrefs.SetInt("GridXValue", _levels[levelNumber-1].gridWidth);
        PlayerPrefs.SetInt("GridYValue", _levels[levelNumber-1].gridHeight);
        int currentLevel = levelNumber++;
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        Debug.Log("CurrentLevel is " + currentLevel);
        SceneManager.LoadScene(1);
    }
    public void Back() {
        mainMenuPanel.SetActive(true);
        levelSelctionPanel.SetActive(false);
    }
    public void UnlockAllLevels() {
        PlayerPrefs.SetInt("levelsUnlocked", 6);
        GoToLevelSelection();
    }
}
[Serializable]
public class Levels {
    public int gridWidth;
    public int gridHeight;
}

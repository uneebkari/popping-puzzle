using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickCheck : MonoBehaviour
{
    RaycastHit raycastHit;
    void Start()
    {
        
    }
    public void CurrentClickedGameObject(GameObject gameObject)
    {
        if (gameObject.tag == "hexagon")
        {
            CancelInvoke();
            StartCoroutine(gameObject.GetComponent<RotateGameObject>().PlayerSwing());
        }
    }
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out raycastHit, 100f))
            {
                if (raycastHit.transform != null)
                {
                    //Debug.Log(raycastHit.transform.name);
                    //Our custom method. 
                    CurrentClickedGameObject(raycastHit.transform.gameObject);
                }
            }
        }
    }
}

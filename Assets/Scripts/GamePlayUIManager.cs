using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlayUIManager : MonoBehaviour
{
    public GameObject levelCompletePanel;
    public Text levelNumberDetails;

    private void Start()
    {
        int currentLevel = PlayerPrefs.GetInt("currentLevel");
        levelNumberDetails.text = "Level : " + currentLevel.ToString();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene(0);

    }
    public void Continue()
    {
        UIManager.isComingForLevelSelction = true;
        SceneManager.LoadScene(0);

    }
    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

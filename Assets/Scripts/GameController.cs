using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController instance = null;
    public GamePlayUIManager uiManager;

    public int totalNumberOfBallToPop = 0;
    public int ballsPoped = 0;
    public bool isGameStarted = false;
    public AudioClip popSound;

    private void Awake()
    {
        instance = this;
    }

    public void PlayPopSfx()
    {
        GetComponent<AudioSource>().PlayOneShot(popSound);
    }
    void LevelCompleted() {
        uiManager.levelCompletePanel.SetActive(true);

    }
    void Update()
    {
        if (isGameStarted && totalNumberOfBallToPop == ballsPoped) {
            isGameStarted = false;
            int unlockedLevel = PlayerPrefs.GetInt("levelsUnlocked");
            int currentLevel = PlayerPrefs.GetInt("currentLevel");
            Debug.Log("Unlock Level Number is "+ unlockedLevel);
            Debug.Log("Cuurent Level Number is "+ currentLevel);


            if (PlayerPrefs.GetInt("levelsUnlocked") == PlayerPrefs.GetInt("currentLevel"))
            {
                unlockedLevel++;
                Debug.Log(unlockedLevel + "    <<<<<<<<<<<<<<<");
                PlayerPrefs.SetInt("levelsUnlocked", unlockedLevel);
            }
            Invoke("LevelCompleted",1f);
            Debug.Log("Level Complete");
        }
    }
}

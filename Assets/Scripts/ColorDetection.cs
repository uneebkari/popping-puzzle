using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorDetection : MonoBehaviour
{
    bool flag = true;

   

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == gameObject.tag && flag)
        {
            other.collider.enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            flag = false;
            GameController.instance.ballsPoped++;
            GetComponent<Animator>().enabled = true;
            GameController.instance.PlayPopSfx();

            //Debug.Log(">>>>> Matched <<<<<");
        }
    }
    void DiableGameObjectAfterAnimation() {
        gameObject.SetActive(false);
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateGameObject : MonoBehaviour
{

	public float[] rotationAngles;
    public Collider[] colliders;
    public int lastAngle;
    void Awake()
    {
        //float angle = rotationAngles[Random.Range(0, rotationAngles.Length)];
        //transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, angle, 0);

    }
    void Start()
    {
        lastAngle = (int)transform.eulerAngles.y;

    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.K))
        {
            StartCoroutine(PlayerSwing());
        }
        //Check for mouse click 
        
    }
    private void EnableColliders()
    {
        //CancelInvoke("EnableColliders");
        foreach (var item in colliders)
        {
            item.enabled=true;
        }
    }
    public IEnumerator PlayerSwing()
    {
        foreach (var item in colliders)
        {
            item.enabled = false;
        }
        transform.DORotate(new Vector3(-90f, lastAngle + 60f, 0f), .3f, RotateMode.Fast);
        //transform.localEulerAngles = new Vector3(-90f, lastAngle + 60f, 0f);
        lastAngle += (int)60f;
        Invoke("EnableColliders",.75f);
        yield return null;
    }
 

}
